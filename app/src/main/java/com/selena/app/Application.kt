package com.selena.app

import android.app.Application
import com.selena.app.di.viewModelModule
import com.selena.app.di.appModule
import com.selena.app.di.activityModule
import com.selena.app.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level


class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            // use AndroidLogger as Koin Logger - default Level.INFO
            androidLogger(Level.INFO)

            // use the Android context given there
            androidContext(this@Application)

            // load properties from assets/koin.properties file
            androidFileProperties()

            // module list
            modules(listOf(appModule, networkModule, viewModelModule, activityModule))
        }
    }
}
