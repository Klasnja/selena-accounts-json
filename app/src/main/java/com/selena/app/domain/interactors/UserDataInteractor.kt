package com.selena.app.domain.interactors

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.selena.app.data.api.models.Account
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.UserAccountsData
import com.selena.app.data.api.models.responses.AccountTransactionsData
import com.selena.app.domain.repositories.UserDataRepository
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.domain.utills.Dispatchers
import kotlinx.coroutines.*
import java.lang.RuntimeException


class UserDataInteractor(private val userDataRepository: UserDataRepository,
                         logRepository: LogRepository,
                         dispatchers: Dispatchers,
                         gson: Gson) : BaseInteractor(logRepository, dispatchers, gson) {


    private var cachedUserAccountsData: ApiModel<UserAccountsData>? = null
    private var cachedAccountTransactionsData: MutableMap<Long, ApiModel<AccountTransactionsData>> = mutableMapOf()

    suspend fun populateUserData(data: MutableLiveData<ApiModel<UserAccountsData>>) {

        val tasks = mutableListOf<Deferred<Any>>()
        coroutineScope {
            if (cachedUserAccountsData != null) {
                data.postValue(cachedUserAccountsData)
            } else {
                val getAccountsTask = async(dispatchers.io()) {
                    logRepository.e("Entry", "   'api call': I'm working in thread ${Thread.currentThread().name}")
                    val result = getUserAccounts()
                    addToCache(result)
                    data.postValue(result)
                }
                tasks.add(getAccountsTask)
                getAccountsTask.await()
            }

            data.value?.model?.apply {
                accounts.forEach { account ->

                    val accountId = account.id
                    val cachedData = cachedAccountTransactionsData.get(accountId)
                    if (cachedData == null) {
                        val getTransactionsTask = async(dispatchers.io()) {
                            logRepository.e("Entry", "   'api call': I'm working in thread ${Thread.currentThread().name}")

                            val result = getTransactionsForAccount(accountId)
                            addToCache(accountId, result)
                        }
                        tasks.add(getTransactionsTask)
                    }
                }
            }

            tasks.awaitAll()
        }
    }

    suspend fun populateAccountData(accountId: Long, data: MutableLiveData<ApiModel<Account>>) {
        getAccountFromCache(accountId)?.let {
            data.postValue(ApiModel(it))
        } ?: run {
            coroutineScope {
                val getAccountsTask = async(dispatchers.io()) {
                    logRepository.e("Entry", "   'api call': I'm working in thread ${Thread.currentThread().name}")
                    val result = getUserAccounts()
                    addToCache(result)
                    val account = getAccountFromCache(accountId)
                    val value = account?.let { ApiModel(it) }
                            ?: run { ApiModel<Account>(null, throwable = RuntimeException("No Such account")) }
                    data.postValue(value)
                }
                getAccountsTask.await()
            }
        }

    }

    fun getAccountFromCache(accountId: Long) = cachedUserAccountsData?.model?.accounts?.firstOrNull { it.id == accountId }


    suspend fun populateAccountTransactionsData(accountId: Long, data: MutableLiveData<ApiModel<AccountTransactionsData>>) {
        val cachedData = cachedAccountTransactionsData.get(accountId)
        if (cachedData != null) {
            data.postValue(cachedData)
        } else {
            coroutineScope {
                val getTransactionsTask = async(dispatchers.io()) {
                    logRepository.e("Entry", "   'api call': I'm working in thread ${Thread.currentThread().name}")
                    val result = getTransactionsForAccount(accountId)
                    addToCache(accountId, result)
                    data.postValue(result)
                }
                getTransactionsTask.await()
            }
        }
    }

    private fun addToCache(result: ApiModel<UserAccountsData>) {
        cachedUserAccountsData = result
    }

    private fun addToCache(id: Long, transactionData: ApiModel<AccountTransactionsData>) {
        synchronized(cachedAccountTransactionsData) {
            cachedAccountTransactionsData[id] = transactionData
        }
    }

    private suspend fun getUserAccounts(): ApiModel<UserAccountsData> =
            try {
                ApiModel(userDataRepository.getUserAccounts())
            } catch (throwable: Throwable) {
                ApiModel(throwable = throwable)
            }


    private suspend fun getTransactionsForAccount(id: Long): ApiModel<AccountTransactionsData> =
            try {
                ApiModel(userDataRepository.getTransactionsForAccount(id))
            } catch (throwable: Throwable) {
                ApiModel(throwable = throwable)
            }

}
