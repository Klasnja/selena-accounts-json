package com.selena.app.domain


class Constants {

    class ApiCallKey {
        companion object {
            const val ERROR_CODE_VALIDATION_FAILS_MIN = 400
            const val ERROR_CODE_VALIDATION_FAILS_MAX = 499
        }
    }
}