package com.selena.app.domain.helpers

import com.google.gson.Gson
import com.selena.app.domain.exceptions.ApplicationException
import com.selena.app.domain.exceptions.ServerApiException
import okhttp3.Response

class ErrorBodyParser {

    companion object {
        fun parse(gson: Gson, response: Response): ApplicationException {
            val responseCode = response.code()
            var responseBody = ""
            return try {
                responseBody = response.body()!!.string()
                val bodyMessage = "get from body"
                ApplicationException(ServerApiException(), responseCode = responseCode, bodyMessage = bodyMessage)
            } catch (throwable: Throwable) {
                ApplicationException(throwable)
            }
        }
    }
}