package com.selena.app.domain.repositories

interface PersistentRepository {

    fun saveBoolean(key: String, value: Boolean)

    fun getBoolean(key: String, default: Boolean) : Boolean

    fun saveData(key: String, value: String?)

    fun getData(key: String, default: String?) : String?

    fun saveStringSet(key: String, value: Set<String>?)

    fun getStringSet(key: String, default: Set<String>?): Set<String>?
}