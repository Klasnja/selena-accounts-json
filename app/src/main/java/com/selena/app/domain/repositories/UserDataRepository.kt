package com.selena.app.domain.repositories

import com.selena.app.data.api.models.UserAccountsData
import com.selena.app.data.api.models.responses.AccountTransactionsData

interface UserDataRepository {

    suspend fun getUserAccounts(): UserAccountsData

    suspend fun getTransactionsForAccount(id: Long): AccountTransactionsData
}