package com.selena.app.di

import com.selena.app.presentation.launch.UserAccountsViewModel
import com.selena.app.presentation.transactions.TransactionAccountsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { UserAccountsViewModel(get(), get(), get()) }

    viewModel { TransactionAccountsViewModel(get(), get(), get()) }
}
