package com.selena.app.di

import com.selena.app.data.managers.AuthManager
import com.selena.app.data.managers.UserDataManager
import com.selena.app.data.managers.LogManager
import com.selena.app.data.managers.PersistentManager
import com.selena.app.domain.repositories.AuthRepository
import com.selena.app.di.NetworkModule.Companion.PRE_INIT_SERVICE_API
import com.selena.app.domain.interactors.UserDataInteractor
import com.selena.app.domain.repositories.UserDataRepository
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.domain.repositories.PersistentRepository
import com.selena.app.domain.utills.Dispatchers
import com.selena.app.util.Logger
import com.selena.app.util.EventBus
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module

@UseExperimental(ExperimentalCoroutinesApi::class)
val appModule = module {

    // region single
    single { AuthManager(get(named(PRE_INIT_SERVICE_API))) } bind AuthRepository::class

    single { UserDataManager(androidContext(), get(), get()) } bind UserDataRepository::class

    single { LogManager()} bind LogRepository::class

    single { PersistentManager(androidContext(), "user.data")} bind PersistentRepository::class

    single { Logger()}

    single { EventBus()}

    single { Dispatchers()}
    // end region

    single { UserDataInteractor(get(), get(), get(), get()) }
}