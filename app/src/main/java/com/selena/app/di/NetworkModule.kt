package com.selena.app.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.selena.app.config.LocalProperties
import com.selena.app.data.api.interceptors.PostLoginInterceptor
import com.selena.app.data.api.interceptors.PreLoginInterceptor
import com.selena.app.data.api.services.AuthApiService
import com.selena.app.data.api.services.EarthquakeService
import com.selena.app.di.NetworkModule.Companion.POST_INIT_OKHTTP
import com.selena.app.di.NetworkModule.Companion.POST_INIT_RETROFIT
import com.selena.app.di.NetworkModule.Companion.POST_INIT_SERVICE_API
import com.selena.app.di.NetworkModule.Companion.PRE_INIT_EARTHQUAKE_SERVICE_API
import com.selena.app.di.NetworkModule.Companion.PRE_INIT_OKHTTP
import com.selena.app.di.NetworkModule.Companion.PRE_INIT_RETROFIT
import com.selena.app.di.NetworkModule.Companion.PRE_INIT_SERVICE_API
import com.selena.app.di.NetworkModule.Companion.buildRetrofit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    factory {
        LocalProperties()
    }

    factory<Gson> {
        GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
                .create()
    }

    factory {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE

        if (get<LocalProperties>().isLoggingEnabled) {
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }
        loggingInterceptor
    }

    single {
        PreLoginInterceptor(get(), get())
    }

    single {
        PostLoginInterceptor(get())
    }

    single<OkHttpClient>(named(PRE_INIT_OKHTTP)) {
        val localProperties = get<LocalProperties>()
        val builder = OkHttpClient().newBuilder()
                .addInterceptor(get<PreLoginInterceptor>())
                .addInterceptor(get<HttpLoggingInterceptor>())
                .connectTimeout(localProperties.apiConnectTimeout, TimeUnit.SECONDS)
                .readTimeout(localProperties.apiReadTimeout, TimeUnit.SECONDS)
                .writeTimeout(localProperties.apiWriteTimeout, TimeUnit.SECONDS)
        builder.build()
    }

    single<OkHttpClient>(named(POST_INIT_OKHTTP)) {
        val localProperties = get<LocalProperties>()
        val builder = OkHttpClient().newBuilder()
                .addInterceptor(get<PostLoginInterceptor>())
                .addInterceptor(get<HttpLoggingInterceptor>())
                .connectTimeout(localProperties.apiConnectTimeout, TimeUnit.SECONDS)
                .readTimeout(localProperties.apiReadTimeout, TimeUnit.SECONDS)
                .writeTimeout(localProperties.apiWriteTimeout, TimeUnit.SECONDS)
        builder.build()
    }

    single(named(PRE_INIT_RETROFIT)) {
        val okHttpClient = get<OkHttpClient>(named(PRE_INIT_OKHTTP))
        buildRetrofit(okHttpClient, get(), get())
    }

    single(named(POST_INIT_RETROFIT)) {
        val okHttpClient = get<OkHttpClient>(named(POST_INIT_OKHTTP))
        buildRetrofit(okHttpClient, get(), get())
    }


    single(named(PRE_INIT_SERVICE_API)) {
        NetworkModule.buildAppApi(get(named(PRE_INIT_RETROFIT)), AuthApiService::class.java)
    }

    single(named(POST_INIT_SERVICE_API)) {
        NetworkModule.buildAppApi(get(named(POST_INIT_RETROFIT)), AuthApiService::class.java)
    }


    single(named(PRE_INIT_EARTHQUAKE_SERVICE_API)) {
        NetworkModule.buildAppApi(get(named(PRE_INIT_RETROFIT)), EarthquakeService::class.java)
    }
}

class NetworkModule {

    companion object {
        const val POST_INIT_OKHTTP = "POST_INIT_OKHTTP"
        const val PRE_INIT_OKHTTP = "PRE_INIT_OKHTTP"

        const val POST_INIT_RETROFIT = "POST_INIT_RETROFIT"
        const val PRE_INIT_RETROFIT = "PRE_INIT_RETROFIT"

        const val POST_INIT_SERVICE_API = "POST_INIT_SERVICE_API"
        const val PRE_INIT_SERVICE_API = "PRE_INIT_SERVICE_API"

        const val PRE_INIT_EARTHQUAKE_SERVICE_API = "PRE_INIT_EARTHQUAKE_SERVICE_API"

        fun buildRetrofit(okHttpClient: OkHttpClient,
                          localProperties: LocalProperties,
                          gson: Gson): Retrofit.Builder {
            return Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(localProperties.baseURL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
        }

        fun <T> buildAppApi(retrofitBuilder: Retrofit.Builder, service: Class<out T>): T =
                retrofitBuilder
                        .build()
                        .create(service)
    }
}