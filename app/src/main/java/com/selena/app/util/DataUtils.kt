package com.selena.app.util

import java.text.SimpleDateFormat
import java.util.*

fun Date.formatDateMMYYYY(timeZone: String? = null): String {
    val dateFormat = SimpleDateFormat("MMM yyyy", Locale.US)
    dateFormat.timeZone = getTimeZone(timeZone)
    return dateFormat.format(this.time)
}

fun Date.formatDateDD(timeZone: String? = null): String {
    val dateFormat = SimpleDateFormat("dd", Locale.US)
    dateFormat.timeZone = getTimeZone(timeZone)
    return dateFormat.format(this.time)
}

fun getTimeZone(timeZone: String?) = timeZone?.let { TimeZone.getTimeZone(it) }
        ?: TimeZone.getDefault()
