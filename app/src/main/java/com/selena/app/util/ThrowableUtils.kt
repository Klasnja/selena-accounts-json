package com.selena.app.util

import com.google.gson.JsonParseException
import com.selena.app.domain.Constants.ApiCallKey.Companion.ERROR_CODE_VALIDATION_FAILS_MAX
import com.selena.app.domain.Constants.ApiCallKey.Companion.ERROR_CODE_VALIDATION_FAILS_MIN
import com.selena.app.domain.exceptions.ApplicationException

fun getErrorOfType(parent: Throwable?, clazz: Class<out Throwable>): Throwable? {
    var throwable: Throwable? = parent ?: return null
    do {
        if (clazz.isAssignableFrom(throwable!!::class.java)) return throwable
        throwable = throwable.cause
    } while (throwable != null)
    return null
}

fun shouldFailOnError(throwable: Throwable): Boolean {
    val appError = getErrorOfType(throwable, ApplicationException::class.java)
    val jsonParsingError = getErrorOfType(throwable, JsonParseException::class.java)
    val illegalArgumentException = getErrorOfType(throwable, IllegalArgumentException::class.java)
    return (jsonParsingError != null) || (illegalArgumentException != null) ||
            (appError != null && (appError as ApplicationException).responseCode in
                    (ERROR_CODE_VALIDATION_FAILS_MIN..ERROR_CODE_VALIDATION_FAILS_MAX))
}

