package com.selena.app.data.managers

import android.content.Context
import com.selena.app.data.api.models.requests.LoginWithEmailRequest
import com.selena.app.data.api.models.responses.UserResponseData
import com.selena.app.data.api.services.AuthApiService
import com.selena.app.domain.repositories.AuthRepository
import retrofit2.Call

class AuthManager constructor(private val preLoginAuthApiService: AuthApiService) : AuthRepository {

    /**
     * Login user with email
     */
    override fun loginWithEmail(email: String, password: String): Call<UserResponseData> {
        return preLoginAuthApiService.loginWithEmail(LoginWithEmailRequest( email, password))
    }
}