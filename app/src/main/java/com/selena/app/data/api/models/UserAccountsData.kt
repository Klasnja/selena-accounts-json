package com.selena.app.data.api.models

import com.google.gson.annotations.SerializedName

data class UserAccountsData(@SerializedName("accounts") val accounts: List<Account>)

data class Account(@SerializedName("id") val id: Long,
                   @SerializedName("name") val name: String,
                   @SerializedName("institution") val institution: String,
                   @SerializedName("currency") val currency: String,
                   @SerializedName("current_balance") val currentBalance: Double,
                   @SerializedName("current_balance_in_base") val currentBalanceInBase: Double)
