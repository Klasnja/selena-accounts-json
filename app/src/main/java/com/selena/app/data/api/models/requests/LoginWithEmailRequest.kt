package com.selena.app.data.api.models.requests

import com.google.gson.annotations.SerializedName

data class LoginWithEmailRequest(@SerializedName("email") val email: String,
                                 @SerializedName("password") val password: String)
