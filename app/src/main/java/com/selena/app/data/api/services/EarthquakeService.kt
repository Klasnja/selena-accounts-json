package com.selena.app.data.api.services

import com.selena.app.data.api.models.UserAccountsData
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path


interface EarthquakeService {
    @GET("/fdsnws/event/{type}/application.json")
    suspend fun getEarthData(@Path("type") type: Int): Response<UserAccountsData>

    @GET("/fdsnws/event/{type}/version")
    suspend fun getVersion(@Path("type") type: Int): Response<ResponseBody>
}

