package com.selena.app.data.managers

import android.content.Context
import com.google.gson.Gson
import com.selena.app.R
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.UserAccountsData
import com.selena.app.data.api.models.responses.AccountTransactionsData
import com.selena.app.domain.repositories.UserDataRepository
import java.io.InputStreamReader


class UserDataManager constructor(private val appContext: Context,
                                  private val persistentManager: PersistentManager,
                                  private val gson: Gson) : UserDataRepository {

    companion object {
        const val USER_ACCOUNTS = "USER_ACCOUNTS"
    }

    override suspend fun getUserAccounts(): UserAccountsData {
        val userAccountsJson = persistentManager.getData(USER_ACCOUNTS, null)

        val userAccountsData =
                if (userAccountsJson == null) {
                    val stream = appContext.resources.openRawResource(R.raw.accounts)
                    val reader = InputStreamReader(stream)
                    gson.fromJson(reader, UserAccountsData::class.java)
                } else {
                    gson.fromJson(userAccountsJson, UserAccountsData::class.java)
                }
        return userAccountsData
    }


    override suspend fun getTransactionsForAccount(id: Long): AccountTransactionsData {
        val resourceId = when(id){
            1L -> R.raw.transactions_1
            2L -> R.raw.transactions_2
            3L -> R.raw.transactions_3
            else -> null
        }
        return resourceId?.let{
            val stream = appContext.resources.openRawResource(it)
            val reader = InputStreamReader(stream)
            gson.fromJson(reader, AccountTransactionsData::class.java)
        } ?: run {
            throw RuntimeException("No such account id $id")
        }
    }
}

