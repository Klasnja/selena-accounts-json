package com.selena.app.data.api.interceptors

import com.google.gson.Gson
import com.selena.app.config.LocalProperties
import okhttp3.Interceptor
import okhttp3.Response

class PreLoginInterceptor constructor(private val localProperties: LocalProperties,
                                      private val gson: Gson) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newRequest = request.newBuilder()
        newRequest.addHeader("Authorization", "Bearer public token here")
        return chain.proceed(newRequest.build())
    }
}