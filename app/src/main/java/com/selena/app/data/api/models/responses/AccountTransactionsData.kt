package com.selena.app.data.api.models.responses

import com.google.gson.annotations.SerializedName
import java.util.*

data class AccountTransactionsData(@SerializedName("transactions") val transactions: List<Transaction>)

data class Transaction(@SerializedName("account_id") val accountId: Long,
                       @SerializedName("amount") val amount: Double,
                       @SerializedName("category_id") val categoryId: Long,
                       @SerializedName("date") val date: Date,
                       @SerializedName("description") val description: String,
                       @SerializedName("id") val id: Long
                       )
