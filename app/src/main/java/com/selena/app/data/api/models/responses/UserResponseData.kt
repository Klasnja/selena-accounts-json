package com.selena.app.data.api.models.responses

import com.google.gson.annotations.SerializedName

data class UserResponseData(@SerializedName("token") val token: String,
                            @SerializedName("id") val id: Int)