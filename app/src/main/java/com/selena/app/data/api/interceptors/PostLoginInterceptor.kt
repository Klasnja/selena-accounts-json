package com.selena.app.data.api.interceptors

import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.Response

class PostLoginInterceptor constructor(private val gson: Gson) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newRequest = request.newBuilder()
        newRequest.addHeader("Authorization", "Bearer user token here")
        return chain.proceed(newRequest.build())
    }
}
