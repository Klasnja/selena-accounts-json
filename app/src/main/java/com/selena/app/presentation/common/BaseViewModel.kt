package com.selena.app.presentation.common

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import kotlinx.coroutines.Job

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    val TAG: String = this::class.java.simpleName

    protected val tasksWhileInMemory = mutableListOf<Job>()

    override fun onCleared() {
        tasksWhileInMemory.forEach { it.cancel() }
        super.onCleared()
    }
}