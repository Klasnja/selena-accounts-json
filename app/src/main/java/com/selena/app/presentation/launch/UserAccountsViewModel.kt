package com.selena.app.presentation.launch

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.UserAccountsData
import com.selena.app.domain.interactors.UserDataInteractor
import com.selena.app.domain.utills.Dispatchers
import com.selena.app.presentation.common.BaseViewModel
import kotlinx.coroutines.launch

class UserAccountsViewModel(application: Application,
                            val dispatchers: Dispatchers,
                            val userDataInteractor: UserDataInteractor) : BaseViewModel(application) {

    private val data = MutableLiveData<ApiModel<UserAccountsData>>()

    init {
        refresh()
    }

    fun getFreshData() {
        refresh()
    }

    private fun refresh() {
        tasksWhileInMemory.add(viewModelScope.launch {
            userDataInteractor.populateUserData(data)
        })
    }

    fun getLiveUserDataObservable() = data
}
