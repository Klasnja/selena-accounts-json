package com.selena.app.presentation.transactions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.selena.app.R
import com.selena.app.data.api.models.Account
import com.selena.app.data.api.models.responses.Transaction
import com.selena.app.util.EventBus
import com.selena.app.util.formatDateDD
import kotlinx.android.synthetic.main.layout_account_item.view.*
import kotlinx.android.synthetic.main.layout_title_item.view.txt_title

class TransactionsDataAdapter(val currency: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val viewItemList = mutableListOf<ViewItem>()

    data class ViewItem(val viewType: ViewType, val data: Any)

    enum class ViewType {
        TITLE,
        ITEM,
        DIVIDER,
        ERROR
    }

    fun updateData(itemList: List<ViewItem>) {
        viewItemList.clear()
        viewItemList.addAll(itemList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = when (viewType) {
        ViewType.TITLE.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_title_item, parent, false)
            TitleItemViewHolder(view)
        }
        ViewType.ITEM.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_account_item, parent, false)
            ItemViewHolder(view)
        }
        ViewType.DIVIDER.ordinal -> {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_divider_item, parent, false)
            DividerItemViewHolder(view)
        }

        else -> throw RuntimeException("not view type matched")
    }


    override fun getItemViewType(position: Int): Int = viewItemList[position].viewType.ordinal

    override fun getItemCount() = viewItemList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TitleItemViewHolder -> {
                holder.bindData(viewItemList[position].data as String)
            }
            is ItemViewHolder -> {
                holder.bindData(viewItemList[position].data as Transaction)
            }
            is DividerItemViewHolder -> {
                holder.bindData()
            }
        }
    }


    class TitleItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(title: String) {
            with(itemView) {
                txt_title.text = title
            }
        }
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(transaction: Transaction) {
            with(itemView) {
                val text = "${transaction.date.formatDateDD()}${transaction.description}"
                txt_name.text = text
                val balance = "$currency${transaction.amount}"
                txt_value.text = balance
            }
        }
    }

    class DividerItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData() {
        }
    }
}