package com.selena.app.presentation.components

import android.content.Context
import android.view.WindowManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity

class LoaderComponent constructor(private val context: Context) {

    companion object {
        private const val DIALOG_TAG = "LoaderDialogFragment.DIALOG_TAG"
    }

    fun show(@StringRes messageResId: Int) {

        with(baseActivity()) {
            if (!isFinishing) {
                window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

                // DialogFragment.show() will take care of adding the fragment
                // in a transaction.  We also want to remove any currently showing
                // dialog, so make our own transaction and take care of that here.
                val ft = supportFragmentManager.beginTransaction()
                val prev = supportFragmentManager.findFragmentByTag(DIALOG_TAG)
                if (prev != null) {
                    ft.remove(prev)
                }

                val newFragment = LoaderDialogFragment.newInstance(getString(messageResId))
                ft.add(newFragment, DIALOG_TAG)
                ft.addToBackStack(null)

                ft.commitAllowingStateLoss()
            }
        }
    }

    fun hide() {
        with(baseActivity()) {
            if (!isFinishing) {
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                val dialog = supportFragmentManager.findFragmentByTag(DIALOG_TAG) as LoaderDialogFragment?
                dialog?.dismissAllowingStateLoss()
            }
        }
    }

    private fun baseActivity() = context as AppCompatActivity
}