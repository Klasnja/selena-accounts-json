package com.selena.app.presentation.common

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.selena.app.util.EventBus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.android.ext.android.inject


@Suppress("UNCHECKED_CAST")
abstract class BaseActivity : AppCompatActivity() {

    val eventBus: EventBus by inject()

    protected var saveState: Bundle? = null

    private var alertDialog: AlertDialog? = null
        set(value) {
            field?.dismiss()
            field = value
        }

    val TAG: String = this::class.java.simpleName

    companion object {
        private const val SAVED_STATE = "SAVED_STATE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loadSavedState(savedInstanceState)

    }

    private fun loadSavedState(savedInstanceState: Bundle?) {
        saveState = if (savedInstanceState != null && savedInstanceState.containsKey(SAVED_STATE)) {
            savedInstanceState.getBundle(SAVED_STATE)
        } else {
            Bundle()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBundle(SAVED_STATE, saveState)
    }


    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
        //android.R.id.search -> searchButton()
        }

        return super.onOptionsItemSelected(menuItem)
    }


    private fun navigateToActivity(activityClass: Class<out BaseActivity>,
                                   bundle: Bundle? = null) {
        val intent = Intent(this, activityClass)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        bundle?.let {
            intent.putExtras(bundle)
        }
        startActivity(intent)
        finish()
    }


    fun setCurrentPopupDialog(alertDialog: AlertDialog) {
        dismissAlertDialog()
        this.alertDialog = alertDialog
    }

    protected fun dismissAlertDialog() {
        alertDialog?.let {
            alertDialog = null
        }
    }


    /**
     * This is the job for all coroutines started by this ViewModel.
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val activityModelJob = Job()

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     * Since we pass viewModelJob, you can cancel all coroutines
     * launched by uiScope by calling viewModelJob.cancel()
     */
    protected val uiScope = CoroutineScope(Dispatchers.Main + activityModelJob)

    override fun onDestroy() {
        super.onDestroy()
        activityModelJob.cancel()
    }


}