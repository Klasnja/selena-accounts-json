package com.selena.app.presentation.launch

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.selena.app.R
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.UserAccountsData
import com.selena.app.presentation.common.BaseActivity
import com.selena.app.presentation.components.ErrorComponent
import com.selena.app.presentation.transactions.TransactionsActivity
import kotlinx.android.synthetic.main.activity_launch.*
import kotlinx.android.synthetic.main.activity_launch_content.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf


class LaunchActivity : BaseActivity() {

    private lateinit var adapter: UserAccountsDataAdapter

    private val errorComponent: ErrorComponent by currentScope.inject { parametersOf(this) }

    private val userAccountsViewModel: UserAccountsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        adapter = UserAccountsDataAdapter(eventBus)

        setupRecyclerView()

        swiperefresh.setOnRefreshListener {
            userAccountsViewModel.getFreshData()
        }

        observeViewModel(userAccountsViewModel)

        listenToEvents()
    }

    private fun listenToEvents() {
        val channel = eventBus.asChannel<UserAccountsDataAdapter.AccountClick>()
        uiScope.launch(Dispatchers.Main) {
            for (account in channel) {
                startTransactionActivity(account.id, account.currency)
            }
        }
    }

    private fun startTransactionActivity(id: Long, currency: String) {
        startActivity(TransactionsActivity.intent(this, id, currency))
    }

    private fun configureToolbar(title: String) {

        toolbar?.apply {
            setTitle(title)
            setSupportActionBar(this)
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }


    private fun observeViewModel(userAccountsViewModel: UserAccountsViewModel) {
        userAccountsViewModel.getLiveUserDataObservable().observe(this, Observer<ApiModel<UserAccountsData>> { data ->
            swiperefresh.isRefreshing = false

            data?.apply {
                model?.let { userAccountData ->
                    updateData(userAccountData)
                }
                throwable?.let {
                    errorComponent.handleError(it) {
                        userAccountsViewModel.getFreshData()
                    }
                }
            }
        })
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        rv_data.layoutManager = layoutManager
        rv_data.itemAnimator = DefaultItemAnimator()
        rv_data.isNestedScrollingEnabled = true
        rv_data.setHasFixedSize(false)
        rv_data.adapter = adapter
    }

    private fun updateData(data: UserAccountsData) {
        val viewItemList = mutableListOf<UserAccountsDataAdapter.ViewItem>()

        if (data.accounts.isNotEmpty()) {
            val total = data.accounts.map {
                it.currentBalanceInBase
            }.reduce { acc, balance -> acc + balance }

            val balance = "${data.accounts[0].currency}$total"
            configureToolbar(balance)
        }

        data.accounts.groupBy { it.institution }.forEach { entry ->

            val institution = entry.key
            val accounts = entry.value.sortedBy { it.name }

            viewItemList.add(UserAccountsDataAdapter.ViewItem(UserAccountsDataAdapter.ViewType.TITLE, institution))
            viewItemList.add(UserAccountsDataAdapter.ViewItem(UserAccountsDataAdapter.ViewType.DIVIDER, ""))
            accounts.forEach {
                viewItemList.add(UserAccountsDataAdapter.ViewItem(UserAccountsDataAdapter.ViewType.ITEM, it))
            }
        }

        adapter.updateData(viewItemList)
    }
}

