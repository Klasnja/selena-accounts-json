package com.selena.app.presentation.transactions

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.selena.app.R
import com.selena.app.data.api.models.Account
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.responses.AccountTransactionsData
import com.selena.app.presentation.common.BaseActivity
import com.selena.app.presentation.components.ErrorComponent
import com.selena.app.util.formatDateMMYYYY
import kotlinx.android.synthetic.main.activity_scrolling.toolbar
import kotlinx.android.synthetic.main.content_scrolling.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf

class TransactionsActivity : BaseActivity() {

    private lateinit var adapter: TransactionsDataAdapter

    private val errorComponent: ErrorComponent by currentScope.inject { parametersOf(this) }

    private val viewModel: TransactionAccountsViewModel by viewModel()

    private var currency: String? = null

    companion object {
        private const val ACCOUNT_ID = "TransactionsActivity.ACCOUNT_ID"
        private const val CURRENCY = "TransactionsActivity.CURRENCY"
        fun intent(context: Context, id: Long, currency: String): Intent {
            val intent = Intent(context, TransactionsActivity::class.java)
            intent.putExtra(ACCOUNT_ID, id.toString())
            intent.putExtra(CURRENCY, currency)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scrolling)

        adapter = TransactionsDataAdapter(currency ?: "")

        setupRecyclerView()

        getTransactionsData()

        swiperefresh.setOnRefreshListener {
            getTransactionsData()
        }

        observeViewModel(viewModel)
    }

    private fun getTransactionsData() {
        val id = intent.getStringExtra(ACCOUNT_ID).toLong()
        viewModel.getFreshData(id)
    }

    private fun configureToolbar(title: String) {
        toolbar?.apply {
            setTitle(title)
            setSupportActionBar(this)
            setNavigationOnClickListener {
                onBackPressed()
            }
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

    }

    private fun observeViewModel(viewModel: TransactionAccountsViewModel) {
        viewModel.getLiveAccountTransactionsDataObservable().observe(this, Observer<ApiModel<AccountTransactionsData>> { data ->
            swiperefresh.isRefreshing = false
            val id = intent.getStringExtra(ACCOUNT_ID).toLong()

            data?.apply {
                model?.let { accountTransactionsData ->
                    updateData(accountTransactionsData)
                }
                throwable?.let {
                    errorComponent.handleError(it) {
                        viewModel.getFreshData(id)
                    }
                }
            }
        })
        viewModel.getLiveAccountDataObservable().observe(this, Observer<ApiModel<Account>> { data ->
            data?.apply {
                model?.let { account ->
                    configureToolbar(account.name)
                }
            }
        })
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        rv_data.layoutManager = layoutManager
        rv_data.itemAnimator = DefaultItemAnimator()
        rv_data.isNestedScrollingEnabled = true
        rv_data.setHasFixedSize(false)
        rv_data.adapter = adapter
    }


    private fun updateData(accountTransactionsData: AccountTransactionsData) {
        val viewItemList = mutableListOf<TransactionsDataAdapter.ViewItem>()

        accountTransactionsData.apply {
            transactions.sortedByDescending { it.date }.groupBy {
                it.date.formatDateMMYYYY()
            }.forEach { entry ->
                val title = entry.key
                val transactions = entry.value

                viewItemList.add(TransactionsDataAdapter.ViewItem(TransactionsDataAdapter.ViewType.TITLE, title))
                viewItemList.add(TransactionsDataAdapter.ViewItem(TransactionsDataAdapter.ViewType.DIVIDER, ""))
                transactions.forEach {
                    viewItemList.add(TransactionsDataAdapter.ViewItem(TransactionsDataAdapter.ViewType.ITEM, it))
                }
            }
        }
        adapter.updateData(viewItemList)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        finish()
    }
}
