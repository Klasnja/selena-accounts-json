package com.selena.app.presentation.components

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.selena.app.R
import kotlinx.android.synthetic.main.loader_component.*

class LoaderDialogFragment : DialogFragment() {

    companion object {
        const val LOADING_TEXT = "LoaderDialogFragment.LOADING_TEXT"

        fun newInstance(loadingText: String): LoaderDialogFragment {
            val dialog = LoaderDialogFragment()
            val bundle = Bundle()
            bundle.putString(LOADING_TEXT, loadingText)
            dialog.arguments = bundle
            return dialog
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.loader_component, container, false)
    }

    override fun onStart() {
        super.onStart()
        dialog?.apply {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            window.setLayout(width, height)
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getString(LOADING_TEXT)?.let {
            loading_text.text = it
        }
    }
}