package com.selena.app.presentation.transactions

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.selena.app.data.api.models.Account
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.responses.AccountTransactionsData
import com.selena.app.domain.interactors.UserDataInteractor
import com.selena.app.domain.utills.Dispatchers
import com.selena.app.presentation.common.BaseViewModel
import kotlinx.coroutines.launch

class TransactionAccountsViewModel(application: Application,
                                   val dispatchers: Dispatchers,
                                   val userDataInteractor: UserDataInteractor) : BaseViewModel(application) {

    private val data = MutableLiveData<ApiModel<AccountTransactionsData>>()
    private val accountData = MutableLiveData<ApiModel<Account>>()


    fun getFreshData(accountId: Long) {
        refresh(accountId)
    }

    private fun refresh(accountId: Long) {
        tasksWhileInMemory.add(viewModelScope.launch {
            userDataInteractor.populateAccountTransactionsData(accountId, data)
            userDataInteractor.populateAccountData(accountId, accountData)
        })
    }

    fun getLiveAccountTransactionsDataObservable() = data
    fun getLiveAccountDataObservable() = accountData
}
