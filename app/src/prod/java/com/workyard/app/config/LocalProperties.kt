package com.selena.app.config

import javax.inject.Singleton

class LocalProperties constructor(){

    val baseURL = "https://staging-api.something.com/"

    val isLoggingEnabled = true

    val apiConnectTimeout: Long = 30
    val apiReadTimeout: Long = 30
    val apiWriteTimeout: Long = 30
}