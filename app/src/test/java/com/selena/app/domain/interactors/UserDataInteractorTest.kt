package com.selena.app.domain.interactors

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.nhaarman.mockitokotlin2.*
import com.selena.app.data.api.models.Account
import com.selena.app.data.api.models.ApiModel
import com.selena.app.data.api.models.UserAccountsData
import com.selena.app.domain.repositories.UserDataRepository
import com.selena.app.domain.repositories.LogRepository
import com.selena.app.util.TestDispatchers
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.*
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.lang.RuntimeException

class UserDataInteractorTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var userDataRepository: UserDataRepository

    @Mock
    lateinit var logRepository: LogRepository

    @Mock
    lateinit var gson: Gson

    private val dispatchers = TestDispatchers()

    private lateinit var interactor: UserDataInteractor

    private val accounts = listOf(Account(1, "name1", "institution",
            "AUD", 222.00, 111.00),
            Account(2, "name2", "institution",
                    "AUD", 44.00, 22.00))
    private val userData = UserAccountsData(accounts)


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        interactor = UserDataInteractor(userDataRepository, logRepository, dispatchers, gson)
    }

    @Test
    fun populateUserData_apiIsSuccessful_resultIsCorrect() = runBlocking {
        // given
        val data = MutableLiveData<ApiModel<UserAccountsData>>()
        data.observeForever {}

        whenever(userDataRepository.getUserAccounts())
                .thenReturn(userData)

        // when
        interactor.populateUserData(data)

        // then
        assertThat(data.value, `is`(notNullValue()))
        assertThat(data.value!!.throwable, `is`(nullValue()))
        assertThat(data.value!!.model, `is`(notNullValue()))
        assertThat(data.value!!.model!!.accounts, `is`(accounts))
    }

    @Test
    fun populateUserData_apiFails_resultIsCorrect() = runBlocking {
        // given
        val data = MutableLiveData<ApiModel<UserAccountsData>>()
        data.observeForever {}

        val exception : Throwable = RuntimeException("An error")
        whenever(userDataRepository.getUserAccounts())
                .thenThrow(exception)

        // when
        interactor.populateUserData(data)

        // then
        assertThat(data.value, `is`(notNullValue()))
        assertThat(data.value!!.model, `is`(nullValue()))
        assertThat(data.value!!.throwable, `is`(notNullValue()))
        assertThat(data.value!!.throwable, `is`(exception))
    }
}